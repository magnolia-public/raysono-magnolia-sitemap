[#compress]
[#assign siteRoot = cmsfn.root(content, "mgnl:page")!content]

[#assign excludedTemplates = ['raysono-magnolia-sitemap:pages/sitemap']]

[#assign total = 0]

[#macro renderSitemap node]
    [#assign children = cmsfn.children(node, 'mgnl:page')![]]

    [#assign hideInSitemap = node.hideInSitemap?? && node.hideInSitemap ]
    [#if !hideInSitemap && !excludedTemplates?seq_contains(cmsfn.metaData(node, "mgnl:template"))]
        [@renderSite node /]
    [/#if]

    [#list children]
        [#items as item]
            [@renderSitemap item /]
        [/#items]
    [/#list]
[/#macro]

[#if !cmsfn.root(content, "mgnl:page")?has_content]
    [#assign root = cmsfn.contentByPath('/', 'website')]
    [@renderSitemap root /]
[#else]
    [@renderSitemap siteRoot /]
[/#if]


[/#compress]
