[#compress]
[#assign header = ctx.response.setHeader("content-type","application/xml")]
<?xml version="1.0" encoding="UTF-8"?>
[#macro renderSite child]
    [#if !(child.noIndexPage?has_content && child.noIndexPage == true)]
        [#assign site = sitefn.site(content)!]
        [#if site?has_content && site.domains?has_content && site.domains?size > 0]
            [#-- TODO - For the moment we only use the first domain --]
            [#assign domain = site.domains[0] /]
            [#assign protocol = domain.protocol /]
            [#assign domainName = domain.name /]
            [#assign port = domain.port /]
            [#if port == 80]
                [#assign port = '' /]
            [/#if]

            [#assign url = protocol?has_content?then(protocol, 'http') + '://' + domainName + port?has_content?then(':' + port, '') /]
        [#else]
            [#assign url = cmsfn.contentByPath("/server", "config").defaultBaseUrl?remove_ending("/")!'']
        [/#if]

        [#if !url?has_content]
            [#assign url = (state.originalBrowserURL?keep_before('?')?remove_ending(state.originalBrowserURI))!'']
        [/#if]
        [#assign link = cmsfn.link(child)!""]
        [#assign linkWithUrl = url + link]
        [#assign slashCount = link?remove_ending("/")?split("/")]
        [#if !(initialSlashCount?has_content)]
            [#assign initialSlashCount = slashCount?size]
        [/#if]
    <url>
        <loc>${linkWithUrl}</loc>
        <changefreq>weekly</changefreq>
    </url>
    [/#if]
[/#macro]

<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    [#include "../../includes/sitemap_macro.ftl"]
</urlset>
[/#compress]
