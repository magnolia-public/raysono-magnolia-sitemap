[#compress]
<!DOCTYPE html>
<html>
[@cms.page /]
<body>
    [#macro renderSite page]
            [#assign site = sitefn.site(content)!]
            [#if site?has_content && site.domains?has_content && site.domains?size > 0]
                [#-- TODO - For the moment we only use the first domain --]
                [#assign domain = site.domains[0] /]
                [#assign protocol = domain.protocol /]
                [#assign domainName = domain.name /]
                [#assign port = domain.port /]
                [#if port == 80]
                    [#assign port = '' /]
                [/#if]

                [#assign url = protocol?has_content?then(protocol, 'http') + '://' + domainName + port?has_content?then(':' + port, '') /]
            [#else]
                [#assign url = cmsfn.contentByPath("/server", "config").defaultBaseUrl?remove_ending("/")!'']
            [/#if]

            [#if !url?has_content]
                [#assign url = (state.originalBrowserURL?keep_before('?')?remove_ending(state.originalBrowserURI))!'']
            [/#if]
            [#assign link = cmsfn.link(page)!'']
            [#assign total += 1]

            ${total}: <a href="${url + link}">${url + link}</a><br>
    [/#macro]

    [#include '../../includes/sitemap_macro.ftl']
</body>
</html>
[/#compress]
