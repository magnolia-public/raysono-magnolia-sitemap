
# Magnolia DXP sitemap module

Adds sitemap to your website in .html and .xml formats with ease. Sitemaps are used by search engines to better understand the structure of your website.

The page property `hideInSitemap` is taken into account to exclude pages from the sitemap.

## Installation


**Magnolia CLI**


Run in Magnolia's folder:


```bash
mgnl install raysono-magnolia-sitemap
```

**Git**

Clone magnolia [sitemap repository](https://gitlab.com/magnolia-public/raysono-magnolia-sitemap) into Magnolia's light modules folder.

## Usage

  

To add sitemap page follows these steps:

1. Log in to your Magnolia website

2. Create a page under your home page and use “Sitemap” template for this page

3. The Sitemap will be rendered in both .html and .xml formats for you

  

To preview sitemap navigate to url of the page you just created.

  

**Example of sitemap urls for both formats**

* https://www.my-site.com/sitemap.html - .html format

* https://www.my-site.com/sitemap.xml - .xml format

localhost:

* localhost:8080/home/sitemap.html - .html format

* localhost:8080/home/sitemap.xml - .xml format

  

## Configuration

  

### Exclude templates from indexing

  

When excluding templates from indexing, each page on your site with this template will not be included in the sitemap.

To exclude templates from indexing to sitemap open **your-project/light-modules/sitemap/includes/sitemap_macro.ftl** in your favourite IDE and locate this line of code:

  

```freemarker

[#assign excludedTemplates = ['sitemap:pages/sitemap']] // Add names of templates you want to exclude to this array

```

  

### Change url priorities in .xml format

  

In .xml format of sitemap each url has a priority. Highest possible priority is 1, which is the root page. Every other "/" in the url descreases the priority by 0.1.

  

**Example**

* https://www.my-site.com - priority **1**

* https://www.my-site.com/foo - priority **0.9**

* https://www.my-site.com/foo/bar - priority **0.8**

localhost:

* localhost:8080 - priority **1**

* localhost:8080/foo - priority **0.9**

* localhost:8080/foo/bar - priority **0.8**



To configure priority values open **your-project/light-modules/sitemap/templates/pages/sitemap-xml.ftl** in your favourite IDE and locate this line of code:

```freemarker

[#assign priority = (9 - (slashCount?size - initialSlashCount))/10] // Adjust this algorithm to change priority values

```
  

## License

[MIT](https://choosealicense.com/licenses/mit/)
